class HttpExceptions implements Exception {
  final List<dynamic> message;

  HttpExceptions(this.message);

  List<dynamic> get getErrorList {
    return message;
  }
//  @override
//  String toString() {
//    return message;
//  }
}