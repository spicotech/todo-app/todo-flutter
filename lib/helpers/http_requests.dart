import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:connectivity/connectivity.dart';

class HttpRequests {
  static const String baseUrl = 'https://todo-api.spico.tech/api';

  Future<bool> checkInternetConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  Future<Map<String, Object>> postRequest({
    String requestUrl,
    Map<String, Object> body,
    Map<String, Object> headers,
  }) async {
    Map<String, Object> decodedResponse = {};
    final requestBody = json.encode(body);
    final url = (baseUrl + requestUrl);

    print(url);

    final _internetConnected = await checkInternetConnection();

    if (_internetConnected) {
      final response =
          await http.post(url, body: requestBody, headers: headers);

      decodedResponse = json.decode(response.body);
    } else {
      decodedResponse = {
        'errors': [
          'You are not connected to internet. Please connect to a network and try again',
        ],
      };
    }
    return decodedResponse;
  }

  Future<Map<String, Object>> putRequest({
    String requestUrl,
    Map<String, Object> body,
    Map<String, Object> headers,
  }) async {
    Map<String, Object> decodedResponse = {};
    final requestBody = json.encode(body);
    final url = (baseUrl + requestUrl);

    print(url);

    final _internetConnected = await checkInternetConnection();

    if (_internetConnected) {
      final response = await http.put(url, body: requestBody, headers: headers);

      decodedResponse = json.decode(response.body);
    } else {
      decodedResponse = {
        'errors': [
          'You are not connected to internet. Please connect to a network and try again',
        ],
      };
    }
    return decodedResponse;
  }

  Future<Map<String, Object>> getRequest(
      {String requestUrl, Map<String, Object> headers}) async {
    Map<String, Object> decodedResponse = {};
    final url = (baseUrl + requestUrl);

    print(url);

    final _internetConnected = await checkInternetConnection();

    if (_internetConnected) {
      final response = await http.get(url, headers: headers);
      final temp = json.decode(response.body);
      if (temp is List) {
        List<dynamic> fetchedData = temp;
        decodedResponse = {
          'fetchedArrayData' : fetchedData,
        };
      } else {
        Map<String, List<dynamic>> errorResponse = {
          'errors' : temp['errors'],
        };
        decodedResponse = errorResponse;
      }
    } else {
      decodedResponse = {
        'errors': [
          'You are not connected to internet. Please connect to a network and try again',
        ],
      };
    }
    return decodedResponse;
  }

//  Future<List<dynamic>> getRequestArrayResponse(
//      {String requestUrl, Map<String, Object> headers}) async {
//    List<dynamic> decodedResponse = [];
//    final url = (baseUrl + requestUrl);
//
//    print(url);
//
//    final _internetConnected = await checkInternetConnection();
//
//    if (_internetConnected) {
//      final response = await http.get(url, headers: headers);
//
//      decodedResponse = json.decode(response.body) as List<dynamic>;
//    } else {
//      decodedResponse = [
//        'You are not connected to internet. Please connect to a network and try again',
//      ];
//    }
//    return decodedResponse;
//  }

  Future<Map<String, Object>> deleteRequest(
      {String requestUrl, Map<String, Object> headers}) async {
    Map<String, Object> decodedResponse = {};
    final url = (baseUrl + requestUrl);

    print(url);

    final _internetConnected = await checkInternetConnection();

    if (_internetConnected) {
      final response = await http.delete(url, headers: headers);
      if (!response.headers['status'].contains('204')) {
        decodedResponse = json.decode(response.body);
      }
    } else {
      decodedResponse = {
        'errors': [
          'You are not connected to internet. Please connect to a network and try again',
        ],
      };
    }
    return decodedResponse;
  }
}
