import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_spicotech/helpers/http_exceptions.dart';
import 'package:todo_spicotech/helpers/size_config.dart';
import 'package:todo_spicotech/providers/tasks_provider.dart';
import 'package:todo_spicotech/widgets/loading_widget.dart';

class TasksScreen extends StatelessWidget {

  static const routeName = '/tasks-screen';

  @override
  Widget build(BuildContext context) {
    final routeArgs = ModalRoute.of(context).settings.arguments as Map<String, Object>;
    final parentListId = routeArgs['listId'];
    final parentListTitle = routeArgs['listTitle'];
    final userAuthToken = routeArgs['userAuthToken'];
    return Scaffold(
      appBar: AppBar(
        title: Text(parentListTitle),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              elevation: 10,
              color: Theme.of(context).primaryColor,
              onPressed: () {
                Provider.of<TasksProvider>(context, listen: false)
                    .tempAddToTasks();
              },
              child: Text(
                '+ Add Task',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: SizeConfig.smallDevice ? 10 : 15,
                ),
              ),
            ),
            Expanded(
              child: ListTasksWidget(
                userAuthToken: userAuthToken,
                parentListId: parentListId,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ListTasksWidget extends StatefulWidget {
  final userAuthToken;
  final parentListId;
  const ListTasksWidget({
    @required this.userAuthToken,
    @required this.parentListId,
  });

  @override
  _ListTasksWidgetState createState() => _ListTasksWidgetState();
}

class _ListTasksWidgetState extends State<ListTasksWidget> {
  Future<void> _refreshUserTasks(
      {String userAuthToken, BuildContext context, int parentListId}) async {
    print('inside refresh user lists, user token: $userAuthToken');

    try {
      await Provider.of<TasksProvider>(context, listen: false)
          .fetchAndSetTasks(userAuthToken: userAuthToken, parentListId: parentListId);
    } on HttpExceptions catch (error) {
      throw error.getErrorList;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _refreshUserTasks(
          userAuthToken: widget.userAuthToken, context: context, parentListId: widget.parentListId,),
      builder: (ctx, listsApiResponseSnapshot) {
        print('inside future builder');
        if (listsApiResponseSnapshot.connectionState ==
            ConnectionState.waiting) {
          print('inside future builder loading');
          return const LoadingWidget();
        }

        print('onto after loading walla');

        if (listsApiResponseSnapshot.hasError) {
          return Column(
            children: <Widget>[
              Text(listsApiResponseSnapshot.error.toString()),
              FlatButton(
                onPressed: () {
                  setState(() {});
                },
                child: Text('refresh'),
              ),
            ],
          );
        }

        if (listsApiResponseSnapshot.connectionState == ConnectionState.done) {
          return Consumer<TasksProvider>(
            builder: (ctx, tasksProvider, _) {
              if (tasksProvider.getUserTasks.length == 0) {
                return Card(
                  elevation: 3,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'You dont seem to have added any Lists...',
                      style: TextStyle(
                        fontSize: SizeConfig.smallDevice ? 10 : 15,
                        fontStyle: FontStyle.italic,
                        color: Colors.grey[700],
                      ),
                    ),
                  ),
                );
              } else {
                return ListView.builder(
                  itemCount: tasksProvider.getUserTasks.length,
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (ctx, index) {
                    return Card(
                      elevation: 3,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          tasksProvider.getUserTasks[index]['title'],
                          style: TextStyle(
                            fontSize: SizeConfig.smallDevice ? 10 : 15,
                            fontStyle: FontStyle.italic,
                            color: Colors.grey[700],
                          ),
                        ),
                      ),
                    );
                  },
                );
              }
            },
          );
        } else {
          return const Text('no data');
        }
      },
    );
  }
}
