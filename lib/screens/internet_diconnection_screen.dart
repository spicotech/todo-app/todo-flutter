import 'package:flutter/material.dart';

class InternetDisconnectionScreen extends StatelessWidget {
  const InternetDisconnectionScreen();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('SimplyEATS'),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Icon(
                Icons.cloud_off,
                size: 60,
              ),
              SizedBox(
                height: 20,
              ),
              const Text(
                'Oops, looks like you aren\'t connected to internet. Please check your network connection and try again :(',
                overflow: TextOverflow.fade,
                style: const TextStyle(fontSize: 22),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
