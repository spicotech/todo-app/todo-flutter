import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_spicotech/helpers/size_config.dart';
import 'package:todo_spicotech/providers/auth_provider.dart';
import 'package:todo_spicotech/helpers/http_exceptions.dart';
import 'package:todo_spicotech/screens/lists_screen.dart';
import 'package:todo_spicotech/screens/sign_up_screen.dart';
import 'package:todo_spicotech/widgets/error_list_widget.dart';
import 'package:todo_spicotech/widgets/loading_widget.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = '/login-screen';
  const LoginScreen();
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  final _emailFocusNode = FocusNode();

  final _passwordFocusNode = FocusNode();

  TextEditingController _emailController = TextEditingController();

  TextEditingController _passwordController = TextEditingController();

  //using constant function literals to make TextFormField widgets constant
  Widget _emailIdTextFieldWidget;
  Widget _passwordTextFieldWidget;

  //getter for LoginId text field widget
  get emailIdTextFieldWidget {
    if (_emailIdTextFieldWidget == null)
      _emailIdTextFieldWidget = TextFormField(
        controller: _emailController,
        validator: (_) {
          if (_emailController.text == null || _emailController.text.isEmpty) {
            return 'Must Enter your Username/Email/Password';
          }
          return null;
        },
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (_) {
          FocusScope.of(context).requestFocus(_passwordFocusNode);
        },
        decoration: const InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.lightBlue,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.lightBlue,
            ),
          ),
          hintText: 'example@email.com',
          labelText: 'Email',
        ),
      );
    return _emailIdTextFieldWidget;
  }

  //getter for LoginId text field widget
  get passwordTextFieldWidget {
    if (_passwordTextFieldWidget == null)
      _passwordTextFieldWidget = TextFormField(
        controller: _passwordController,
        focusNode: _passwordFocusNode,
        validator: (_) {
          if (_passwordController.text == null ||
              _passwordController.text.isEmpty) {
            return 'Must Enter your Password';
          }
          return null;
        },
        obscureText: true,
        decoration: const InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.lightBlue,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.lightBlue,
            ),
          ),
          labelText: 'Password',
        ),
      );
    return _passwordTextFieldWidget;
  }

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  Future<void> _submitRequest() async {
    if (!_formKey.currentState.validate()) {
      return;
    }

    _showLoadingAlert();

    try {

      print('before trimming email:${_emailController.text}');
      print('before trimming password:${_passwordController.text}');

      final _email = _emailController.text.trim();
      final _password = _passwordController.text.trim();
      await Provider.of<AuthProvider>(context, listen: false)
          .submitSignInRequest(
        email: _email,
        password: _password,
      );
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacementNamed(ListsScreen.routeName);
    } on HttpExceptions catch (error) {
      Navigator.of(context).pop();
      _showErrorDialogue(error.getErrorList);
      print('error situation');
    }
  }

  void _showErrorDialogue(List<dynamic> errorMessages) {
    showDialog(
      context: context,
      builder: (ctx) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: ErrorListWidget(errorMessages: errorMessages),
      ),
    );
  }

  void _showLoadingAlert() {
    showDialog(
      context: context,
      builder: (ctx) => const LoadingWidget(),
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
        currentFocus.unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Welcome to ToDo App',
            style: TextStyle(
              color: Theme.of(context).primaryColorLight,
            ),
          ),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizeConfig.smallDevice
                    ? const SizedBox(
                        height: 30,
                      )
                    : const SizedBox(
                        height: 40,
                      ),
                SizeConfig.smallDevice ? Text(
                  'Login to your account',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey[700],
                  ),
                ) : Text(
                  'Login to your account',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.grey[700],
                  ),
                ),
                SizeConfig.smallDevice
                    ? const SizedBox(
                        height: 30,
                      )
                    : const SizedBox(
                        height: 40,
                      ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      emailIdTextFieldWidget,
                      SizeConfig.smallDevice
                          ? const SizedBox(
                              height: 10,
                            )
                          : const SizedBox(
                              height: 20,
                            ),
                      passwordTextFieldWidget,
                    ],
                  ),
                ),
                SizeConfig.smallDevice
                    ? const SizedBox(
                        height: 40,
                      )
                    : const SizedBox(
                        height: 50,
                      ),
                SizeConfig.smallDevice
                    ? SignInButtonSmall(
                        submit: _submitRequest,
                      )
                    : SignInButtonBig(
                        submit: _submitRequest,
                      ),
                SizeConfig.smallDevice
                    ? const SizedBox(
                        height: 20,
                      )
                    : const SizedBox(
                        height: 30,
                      ),
                const SignUpInsteadRowWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class AppBarBackButton extends StatelessWidget {
  const AppBarBackButton();
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {},
//          Navigator.of(context).pushReplacementNamed(HomeScreen.routeName),
    );
  }
}

class SignInButtonSmall extends StatelessWidget {
  final Function submit;
  const SignInButtonSmall({this.submit});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 10,
      ),
//              height: deviceHeight * 0.07,
      child: FlatButton(
        color: Theme.of(context).primaryColor,
        onPressed: submit,
        child: const Text(
          'Sign In',
          style: TextStyle(
            color: Colors.white,
            fontSize: 10,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

class SignInButtonBig extends StatelessWidget {
  final Function submit;
  const SignInButtonBig({this.submit});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 10,
      ),
//              height: deviceHeight * 0.07,
      child: FlatButton(
        color: Theme.of(context).primaryColor,
        onPressed: submit,
        child: const Text(
          'Sign In',
          style: TextStyle(
            color: Colors.white,
            fontSize: 15,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

class SignUpInsteadRowWidget extends StatelessWidget {
  const SignUpInsteadRowWidget();
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text('Dont have an account? '),
        const SignUpInsteadButton(),
        const Text(' instead!'),
      ],
    );
  }
}

class SignUpInsteadButton extends StatelessWidget {
  const SignUpInsteadButton();
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      constraints: const BoxConstraints(),
      onPressed: () {
        Navigator.of(context).pushReplacementNamed(SignUpScreen.routeName);
      },
      child: Text(
        'SIGN UP',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Theme.of(context).primaryColor,
        ),
      ),
    );
  }
}



















