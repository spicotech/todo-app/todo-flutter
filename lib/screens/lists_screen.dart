import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_spicotech/helpers/size_config.dart';
import 'package:todo_spicotech/providers/auth_provider.dart';
import 'package:todo_spicotech/helpers/http_exceptions.dart';
import 'package:todo_spicotech/providers/lists_provider.dart';
import 'package:todo_spicotech/screens/login_screen.dart';
import 'package:todo_spicotech/screens/tasks_screen.dart';
import 'package:todo_spicotech/widgets/error_list_widget.dart';
import 'package:todo_spicotech/widgets/lists_screen_bottom_sheet_widget.dart';
import 'package:todo_spicotech/widgets/loading_widget.dart';

class ListsScreen extends StatelessWidget {
  const ListsScreen();
  static const routeName = '/lists-screen';
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<AuthProvider>(context, listen: false).getUser;
    print('stateless rebuilding');
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(
          '${user['name']}\'s Lists',
          style: TextStyle(
            color: Theme.of(context).primaryColorLight,
          ),
        ),
        actions: <Widget>[
          const SignOutButton(),
        ],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              SizeConfig.smallDevice
                  ? const SizedBox(
                      height: 30,
                    )
                  : const SizedBox(
                      height: 40,
                    ),
              SizeConfig.smallDevice
                  ? Text(
                      'Welcome to TODOS',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.grey[700],
                      ),
                    )
                  : Text(
                      'Welcome to TODOS',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.grey[700],
                      ),
                    ),
              SizeConfig.smallDevice
                  ? const SizedBox(
                      height: 30,
                    )
                  : const SizedBox(
                      height: 40,
                    ),
              const AddNewListButton(),
              SizeConfig.smallDevice
                  ? const SizedBox(
                      height: 30,
                    )
                  : const SizedBox(
                      height: 40,
                    ),
              const UserListsListViewWidget(),
            ],
          ),
        ),
      ),
    );
  }
}

class SignOutButton extends StatelessWidget {
  const SignOutButton();

  Future<void> _submitRequest(BuildContext context) async {
    _showLoadingAlert(context);

    try {
      await Provider.of<AuthProvider>(context, listen: false)
          .submitLogOutRequest();
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
    } on HttpExceptions catch (error) {
      Navigator.of(context).pop();
      _showErrorDialogue(error.getErrorList, context);
    }
  }

  void _showErrorDialogue(List<dynamic> errorMessages, BuildContext context) {
    showDialog(
      context: context,
      builder: (ctx) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: ErrorListWidget(errorMessages: errorMessages),
      ),
    );
  }

  void _showLoadingAlert(BuildContext context) {
    showDialog(
      context: context,
      builder: (ctx) => const LoadingWidget(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: () => _submitRequest(context),
      child: Row(
        children: <Widget>[
          Text(
            'Sign Out',
            style: TextStyle(
              color: Theme.of(context).primaryColorLight,
            ),
          ),
          Icon(
            Icons.exit_to_app,
            color: Theme.of(context).primaryColorLight,
          ),
        ],
      ),
    );
  }
}

class AddNewListButton extends StatelessWidget {
  const AddNewListButton();

  void _modalBottomSheetMenu(BuildContext context) {
    showDialog(
      context: context,
      builder: (ctx) => Dialog(
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: const ListScreenBottomSheetWidget(),
      ),
    );
//    showModalBottomSheet(
//      context: context,
//      backgroundColor: Colors.transparent,
//      isScrollControlled: true,
//      builder: (builder) {
//        return ;
//      },
//    );
  }

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      elevation: 10,
      color: Theme.of(context).primaryColor,
      onPressed: () => _modalBottomSheetMenu(context),
      child: Text(
        '+ Add List',
        style: TextStyle(
          color: Colors.white,
          fontSize: SizeConfig.smallDevice ? 10 : 15,
        ),
      ),
    );
  }
}

class UserListsListViewWidget extends StatefulWidget {
  const UserListsListViewWidget();

  @override
  _UserListsListViewWidgetState createState() =>
      _UserListsListViewWidgetState();
}

class _UserListsListViewWidgetState extends State<UserListsListViewWidget> {
  bool _apiReqFinished = false;

  Future<void> _refreshUserLists(
      {String userAuthToken, BuildContext context}) async {
    try {
      await Provider.of<ListsProvider>(context, listen: false)
          .fetchAndSetLists(userAuthToken)
          .then(
        (_) {
          if (Provider.of<ListsProvider>(context, listen: false)
                  .getUserLists
                  .length >
              0) _apiReqFinished = false;
        },
      );
    } on HttpExceptions catch (error) {
      throw error.getErrorList;
    }
  }

  @override
  Widget build(BuildContext context) {
    final userAuthToken = Provider.of<AuthProvider>(context).getToken;
    print(userAuthToken);
    print('stateful rebuilding');
    return !_apiReqFinished
        ? FutureBuilder(
            future: _refreshUserLists(
                userAuthToken: userAuthToken, context: context),
            builder: (ctx, listsApiResponseSnapshot) {
              if (listsApiResponseSnapshot.connectionState ==
                  ConnectionState.done) {
                if (listsApiResponseSnapshot.hasError) {
                  List<dynamic> errorList = listsApiResponseSnapshot.error;
                  return Column(
                    children: <Widget>[
                      ListView.builder(
                        itemCount: errorList.length,
                        shrinkWrap: true,
                        physics: BouncingScrollPhysics(),
                        itemBuilder: (ctx, index) {
                          return Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                10,
                              ),
                            ),
                            elevation: 3,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.error_outline,
                                    color: Colors.red,
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    child: Text(
                                      errorList[index].toString(),
                                      style: TextStyle(
                                        fontSize:
                                            SizeConfig.smallDevice ? 10 : 15,
                                        color: Colors.red,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      RaisedButton(
                        onPressed: () {
                          setState(() {});
                        },
                        child: Text('Refresh'),
                      ),
                    ],
                  );
                }
                return Consumer<ListsProvider>(
                  builder: (ctx, listsProvider, _) {
                    if (listsProvider.getUserLists.length == 0) {
                      return Card(
                        elevation: 3,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            'You dont seem to have added any Lists...',
                            style: TextStyle(
                              fontSize: SizeConfig.smallDevice ? 10 : 15,
                              fontStyle: FontStyle.italic,
                              color: Colors.grey[700],
                            ),
                          ),
                        ),
                      );
                    } else {
                      return Expanded(
                        child: ListView.builder(
                          itemCount: listsProvider.getUserLists.length,
                          shrinkWrap: true,
                          physics: BouncingScrollPhysics(),
                          itemBuilder: (ctx, index) {
                            return GestureDetector(
                              onTap: () => Navigator.of(context).pushNamed(
                                TasksScreen.routeName,
                                arguments: {
                                  'listId': listsProvider.getUserLists[index]
                                      ['id'],
                                  'listTitle': listsProvider.getUserLists[index]
                                      ['name'],
                                  'userAuthToken': userAuthToken,
                                },
                              ),
                              child: Card(
                                elevation: 3,
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    listsProvider.getUserLists[index]['name'],
                                    style: TextStyle(
                                      fontSize:
                                          SizeConfig.smallDevice ? 10 : 15,
                                      fontStyle: FontStyle.italic,
                                      color: Colors.grey[700],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      );
                    }
                  },
                );
              } else {
                if (listsApiResponseSnapshot.hasError) {
                  return Column(
                    children: <Widget>[
                      Text(listsApiResponseSnapshot.error.toString()),
                      FlatButton(
                        onPressed: () {
                          setState(() {});
                        },
                        child: Text('refresh'),
                      ),
                    ],
                  );
                }
                return const LoadingWidget();
              }
            },
          )
        : Consumer<ListsProvider>(
            builder: (ctx, listsProvider, _) {
              if (listsProvider.getUserLists.length == 0) {
                return Card(
                  elevation: 3,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'You dont seem to have added any Lists...',
                      style: TextStyle(
                        fontSize: SizeConfig.smallDevice ? 10 : 15,
                        fontStyle: FontStyle.italic,
                        color: Colors.grey[700],
                      ),
                    ),
                  ),
                );
              } else {
                return Expanded(
                  child: ListView.builder(
                    itemCount: listsProvider.getUserLists.length,
                    shrinkWrap: true,
                    physics: BouncingScrollPhysics(),
                    itemBuilder: (ctx, index) {
                      return GestureDetector(
                        onTap: () => Navigator.of(context).pushNamed(
                          TasksScreen.routeName,
                          arguments: {
                            'listId': listsProvider.getUserLists[index]['id'],
                            'listTitle': listsProvider.getUserLists[index]
                                ['name'],
                            'userAuthToken': userAuthToken,
                          },
                        ),
                        child: Card(
                          elevation: 3,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              listsProvider.getUserLists[index]['name'],
                              style: TextStyle(
                                fontSize: SizeConfig.smallDevice ? 10 : 15,
                                fontStyle: FontStyle.italic,
                                color: Colors.grey[700],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                );
              }
            },
          );
  }
}
