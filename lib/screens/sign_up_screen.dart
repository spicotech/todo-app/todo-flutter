import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_spicotech/helpers/size_config.dart';
import 'package:todo_spicotech/providers/auth_provider.dart';
import 'package:todo_spicotech/helpers/http_exceptions.dart';
import 'package:todo_spicotech/screens/lists_screen.dart';
import 'package:todo_spicotech/screens/login_screen.dart';
import 'package:todo_spicotech/widgets/error_list_widget.dart';
import 'package:todo_spicotech/widgets/loading_widget.dart';

class SignUpScreen extends StatefulWidget {
  static const routeName = '/sign-up-screen';
  const SignUpScreen();
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  final _emailFocusNode = FocusNode();
  final _passwordFocusNode = FocusNode();

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  //using constant function literals to make TextFormField widgets constant
  Widget _nameTextFieldWidget;
  Widget _emailTextFieldWidget;
  Widget _passwordTextFieldWidget;

  //getter for name text field widget
  get nameTextFieldWidget {
    if (_nameTextFieldWidget == null)
      _nameTextFieldWidget = TextFormField(
        controller: _nameController,
        validator: (_) {
          if (_nameController.text == null || _nameController.text.isEmpty) {
            return 'Must Enter your Name';
          }
          return null;
        },
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (_) {
          FocusScope.of(context).requestFocus(_emailFocusNode);
        },
        decoration: const InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.lightBlue,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.lightBlue,
            ),
          ),
          hintText: 'Your Name',
          labelText: 'Name',
        ),
      );
    return _nameTextFieldWidget;
  }

  //getter for email text field widget
  get emailTextFieldWidget {
    if (_emailTextFieldWidget == null)
      _emailTextFieldWidget = TextFormField(
        controller: _emailController,
        focusNode: _emailFocusNode,
        validator: (_) {
          if (_emailController.text == null || _emailController.text.isEmpty) {
            return 'Must Enter your Email';
          }
          return null;
        },
        keyboardType: TextInputType.emailAddress,
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (_) {
          FocusScope.of(context).requestFocus(_passwordFocusNode);
        },
        decoration: const InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.lightBlue,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.lightBlue,
            ),
          ),
          hintText: 'example@email.com',
          labelText: 'Email',
        ),
      );
    return _emailTextFieldWidget;
  }

  //getter for password text field widget
  get passwordTextFieldWidget {
    if (_passwordTextFieldWidget == null)
      _passwordTextFieldWidget = TextFormField(
        controller: _passwordController,
        focusNode: _passwordFocusNode,
        validator: (_) {
          if (_passwordController.text == null ||
              _passwordController.text.isEmpty) {
            return 'Must Enter your Password';
          }
          return null;
        },
        obscureText: true,
        decoration: const InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.lightBlue,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.lightBlue,
            ),
          ),
          labelText: 'Password',
        ),
      );
    return _passwordTextFieldWidget;
  }

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    _emailController.dispose();
    _nameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  Future<void> _submitRequest() async {
    if (!_formKey.currentState.validate()) {
      return;
    }

    _showLoadingAlert();

    try {

      final _name = _nameController.text.trim();
      final _email = _emailController.text.trim();
      final _password = _passwordController.text.trim();
      await Provider.of<AuthProvider>(context, listen: false)
          .submitSignUpRequest(
        name: _name,
        email: _email,
        password: _password,
      );
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacementNamed(ListsScreen.routeName);
    } on HttpExceptions catch (error) {
      Navigator.of(context).pop();
      _showErrorDialogue(error.getErrorList);
    }
  }

  void _showErrorDialogue(List<dynamic> errorMessages) {
    showDialog(
      context: context,
      builder: (ctx) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: ErrorListWidget(errorMessages: errorMessages),
      ),
    );
  }

  void _showLoadingAlert() {
    showDialog(
      context: context,
      builder: (ctx) => const LoadingWidget(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
        currentFocus.unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
//          leading: const AppBarBackButton(),
          title: Text(
            'Welcome to ToDo App',
            style: TextStyle(
              color: Theme.of(context).primaryColorLight,
            ),
          ),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizeConfig.smallDevice
                    ? const SizedBox(
                  height: 30,
                )
                    : const SizedBox(
                  height: 40,
                ),
                SizeConfig.smallDevice ? Text(
                  'Create a new account',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey[700],
                  ),
                ) : Text(
                  'Create a new account',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.grey[700],
                  ),
                ),
                SizeConfig.smallDevice
                    ? const SizedBox(
                  height: 30,
                )
                    : const SizedBox(
                  height: 40,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      nameTextFieldWidget,
                      SizeConfig.smallDevice
                          ? const SizedBox(
                              height: 10,
                            )
                          : const SizedBox(
                              height: 20,
                            ),
                      emailTextFieldWidget,
                      SizeConfig.smallDevice
                          ? const SizedBox(
                              height: 10,
                            )
                          : const SizedBox(
                              height: 20,
                            ),
                      passwordTextFieldWidget,
                    ],
                  ),
                ),
                SizeConfig.smallDevice
                    ? const SizedBox(
                        height: 20,
                      )
                    : const SizedBox(
                        height: 30,
                      ),
                SizeConfig.smallDevice
                    ? SignUpButtonSmall(
                        submit: _submitRequest,
                      )
                    : SignUpButtonBig(
                        submit: _submitRequest,
                      ),
                SizeConfig.smallDevice
                    ? const SizedBox(
                        height: 10,
                      )
                    : const SizedBox(
                        height: 20,
                      ),
                const SignInInsteadRowWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SignUpButtonSmall extends StatelessWidget {
  final Function submit;
  const SignUpButtonSmall({this.submit});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 10,
      ),
//              height: deviceHeight * 0.07,
      child: FlatButton(
        color: Theme.of(context).primaryColor,
        onPressed: submit,
        child: const Text(
          'Sign Up',
          style: TextStyle(
            color: Colors.white,
            fontSize: 10,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

class SignUpButtonBig extends StatelessWidget {
  final Function submit;
  const SignUpButtonBig({this.submit});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 10,
      ),
//              height: deviceHeight * 0.07,
      child: FlatButton(
        color: Theme.of(context).primaryColor,
        onPressed: submit,
        child: const Text(
          'Sign Up',
          style: TextStyle(
            color: Colors.white,
            fontSize: 15,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

class SignInInsteadRowWidget extends StatelessWidget {
  const SignInInsteadRowWidget();
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text('Already have an account? '),
        const SignInInsteadButton(),
        const Text(' instead!'),
      ],
    );
  }
}

class SignInInsteadButton extends StatelessWidget {
  const SignInInsteadButton();
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      constraints: const BoxConstraints(),
      onPressed: () {
        Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
      },
      child: Text(
        'SIGN IN',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Theme.of(context).primaryColor,
        ),
      ),
    );
  }
}
