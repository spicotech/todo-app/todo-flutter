import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/provider.dart';

import 'package:todo_spicotech/helpers/http_requests.dart';
import 'package:todo_spicotech/helpers/size_config.dart';
import 'package:todo_spicotech/providers/auth_provider.dart';
import 'package:todo_spicotech/helpers/http_exceptions.dart';
import 'package:connectivity/connectivity.dart';
import 'package:todo_spicotech/providers/lists_provider.dart';
import 'package:todo_spicotech/providers/tasks_provider.dart';
import 'package:todo_spicotech/screens/internet_diconnection_screen.dart';
import 'package:todo_spicotech/screens/lists_screen.dart';
import 'package:todo_spicotech/screens/login_screen.dart';
import 'package:todo_spicotech/screens/sign_up_screen.dart';
import 'package:todo_spicotech/screens/tasks_screen.dart';
import 'package:todo_spicotech/widgets/error_list_widget.dart';
import 'package:todo_spicotech/widgets/loading_widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: AuthProvider(),
        ),
        ChangeNotifierProvider.value(
          value: ListsProvider(),
        ),
        ChangeNotifierProvider.value(
          value: TasksProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo ToDo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.lightBlue,
        ),
        routes: {
          LoginScreen.routeName: (ctx) => LoginScreen(),
          SignUpScreen.routeName: (ctx) => SignUpScreen(),
          ListsScreen.routeName: (ctx) => ListsScreen(),
          TasksScreen.routeName: (ctx) => TasksScreen(),
        },
        home: LandingPage(),
      ),
    );
  }
}

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? const IOSInternetCheck()
        : const AndroidInternetCheck();
  }
}

class IOSInternetCheck extends StatefulWidget {
  const IOSInternetCheck();
  @override
  _IOSInternetCheckState createState() => _IOSInternetCheckState();
}

class _IOSInternetCheckState extends State<IOSInternetCheck> {
  bool _internetAvailability = false;

//  Connectivity connectivity;
//
//  StreamSubscription<ConnectivityResult> subscription;
//
//  @override
//  void didChangeDependencies() {
//    if (!_internetAvailability) {
//      setState(() {
//        print('init state');
//        _checkInternet();
//      });
//    }
//    super.didChangeDependencies();
//  }
//
  @override
  void initState() {
    if (!_internetAvailability) {
      _checkInternet();
    }

//    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result){
//      print(result);
//        if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
//          setState(() {
//            _internetAvailability = true;
//          });
//        }
//    });
    super.initState();
  }

  Future<void> _checkInternet() async {
    var connectivityResult = await (new Connectivity().checkConnectivity());
    print(connectivityResult);
    print('inside checking internet');
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        _internetAvailability = true;
      });
    }
  }

  @override
  void dispose() {
//    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return !_internetAvailability
        ? Scaffold(
            backgroundColor: Theme.of(context).primaryColorLight,
            body: Center(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Icon(
                      Icons.cloud_off,
                      size: 60,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      'Oops, looks like you aren\'t connected to internet. Please check your network connection and try again :(',
                      overflow: TextOverflow.fade,
                      style: const TextStyle(fontSize: 22),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      elevation: 10,
                      color: Theme.of(context).primaryColor,
                      onPressed: _checkInternet,
                      child: Text(
                        'Retry',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: SizeConfig.smallDevice ? 10 : 15,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        : Consumer<AuthProvider>(
            builder: (ctx, auth, _) => FutureBuilder(
              future: auth.tryAutoLogin(),
              builder: (ctx, authResultSnapshot) {
                if (authResultSnapshot.connectionState ==
                    ConnectionState.done) {
                  if (authResultSnapshot.hasError) {
                    return const LoginScreen();
                  }
                  return const ListsScreen();
                } else {
                  return const LoadingWidget();
                }
              },
            ),
          );
  }
}

class AndroidInternetCheck extends StatefulWidget {
  const AndroidInternetCheck();

  @override
  _AndroidInternetCheckState createState() => _AndroidInternetCheckState();
}

class _AndroidInternetCheckState extends State<AndroidInternetCheck> {
  Future _tryingAutoLogin;

  @override
  void initState() {
    _tryingAutoLogin = _tryAutoLogin();
    super.initState();
  }

  _tryAutoLogin() async {
    return await Provider.of<AuthProvider>(context, listen: false)
        .tryAutoLogin();
  }

  @override
  Widget build(BuildContext context) {
    print("rebuilding");
    return StreamBuilder<ConnectivityResult>(
      stream: Connectivity().onConnectivityChanged,
      builder: (context, connectivitySnapshot) {
        SizeConfig().init(context);
        if (!connectivitySnapshot.hasData) {
          return const LoadingWidget();
        }

        if (connectivitySnapshot.hasError) {
          return ErrorListWidget(
              errorMessages: [connectivitySnapshot.error.toString()]);
        }

        if (connectivitySnapshot.data == ConnectivityResult.mobile ||
            connectivitySnapshot.data == ConnectivityResult.wifi) {
          return FutureBuilder(
              future: _tryingAutoLogin,
              builder: (ctx, authResultSnapshot) {
                print('running future builder again');
                if (authResultSnapshot.connectionState ==
                    ConnectionState.done) {
                  if (authResultSnapshot.data) {
                    return const ListsScreen();

                  } else {
                    return const LoginScreen();
                  }
                } else {
                  return const LoadingWidget();
                }
              },
            );
        } else {
          return const InternetDisconnectionScreen();
        }
      },
    );
  }
}
