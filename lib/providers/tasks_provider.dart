
import 'package:flutter/material.dart';
import 'package:todo_spicotech/helpers/http_exceptions.dart';
import 'package:todo_spicotech/helpers/http_requests.dart';

class TasksProvider with ChangeNotifier {

  int listId;

  List<Map<String, Object>> _userTasks = [];


  List<Map<String, Object>> get getUserTasks {
    return [..._userTasks];
  }

  Future<void> fetchAndSetTasks({@required String userAuthToken, @required int parentListId}) async {

    listId = parentListId;

    final requestUrl = '/lists/$listId/tasks';

    print('inside provider fectching tasks, token: $userAuthToken');

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': userAuthToken,
    };


    try {

      final responseObject = await HttpRequests().getRequest(
        headers: headers,
        requestUrl: requestUrl,
      );


      if (responseObject.containsKey('errors')) {
        var errorList = responseObject['errors'] as List<dynamic>;
        throw HttpExceptions(errorList);
      }


      List<dynamic> _fetchTasks = responseObject['fetchedArrayData'];

      List<Map<String, Object>> _tempListTasks = [];

      _fetchTasks.forEach((singleTask) {
        _tempListTasks.add({
          'id' : singleTask['id'],
          'title' : singleTask['name'],
          'status' : singleTask['status'],
          'createdAt' : singleTask['created_at'],
        });
      });

      _userTasks = _tempListTasks;

      notifyListeners();
    } catch (error) {
      throw error;
    }

  }


  void tempAddToTasks() {
    _userTasks.add({
      'id' : 9999,
      'title' : 'HARD CODED',
      'createdAt' : 'some random datetime',
      'status' : 'todo',
    });
    notifyListeners();
  }

}