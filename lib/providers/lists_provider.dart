
import 'package:flutter/material.dart';
import 'package:todo_spicotech/helpers/http_exceptions.dart';
import 'package:todo_spicotech/helpers/http_requests.dart';

class ListsProvider with ChangeNotifier {

  List<Map<String, Object>> _userLists = [];


  List<Map<String, Object>> get getUserLists {
    return [..._userLists];
  }

  Future<void> fetchAndSetLists(String userAuthToken) async {


    final requestUrl = '/lists';


    final headers = {
      'Content-Type': 'application/json',
      'Authorization': userAuthToken,
    };

    try {

      final responseObject = await HttpRequests().getRequest(
        headers: headers,
        requestUrl: requestUrl,
      );

      if (responseObject.containsKey('errors')) {
        List<dynamic> errorList = responseObject['errors'] as List<dynamic>;
        throw HttpExceptions(errorList);
      }


      List<dynamic> _fetchLists = responseObject['fetchedArrayData'];

      List<Map<String, Object>> _tempMapLists = [];

      _fetchLists.forEach((singleList) {
        _tempMapLists.add({
          'id' : singleList['id'],
          'name' : singleList['name'],
          'createdAt' : singleList['created_at'],
        });
      });

      _userLists = _tempMapLists;

      notifyListeners();
    } catch (error) {
      throw error;
    }

  }


  void tempAddToLists() {
    _userLists.add({
      'id' : 9999,
      'name' : 'HARD CODED',
      'createdAt' : 'some random datetime',
    });
    notifyListeners();
  }

}