import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todo_spicotech/helpers/http_exceptions.dart';
import 'package:todo_spicotech/helpers/http_requests.dart';

class AuthProvider with ChangeNotifier {
  Map<String, Object> _user = {};
  String _authToken = '';

  String get getToken {
//    if (_authToken != null) {
//      return _authToken;
//    } else {
//      return '';
//    }
    return _authToken;
  }

  Map<String, Object> get getUser {
    Map<String, Object> _userData = {
      'authToken': _authToken,
      'userId': _user['id'],
      'name': _user['name'],
      'email': _user['email'],
      'createdAt': _user['created_at'],
    };

    return _userData;
  }

  Future<void> submitSignUpRequest({
    @required String name,
    @required String email,
    @required String password,
  }) async {
    final requestBody = {
      'name': name,
      'email': email,
      'password': password,
    };

    final requestUrl = '/auth/sign_up';

    final headers = {
      'Content-Type': 'application/json',
    };

    try {
      final responseObject = await HttpRequests().postRequest(
        headers: headers,
        body: requestBody,
        requestUrl: requestUrl,
      );
      if (responseObject.containsKey('errors')) {
        var errorList = responseObject['errors'] as List<dynamic>;
        throw HttpExceptions(errorList);
      }
      print(responseObject);
      _authToken = responseObject['Authorization'].toString();

      var _userObject = responseObject['user'] as Map<String, Object>;

      _user = {
        'id': _userObject['id'],
        'email': _userObject['email'],
        'name': _userObject['name'],
        'createdAt': _userObject['created_at'],
      };
      _user = _userObject;

      final prefs = await SharedPreferences.getInstance();
      final _userData = json.encode({
        'Authorization': _authToken,
        'user': _user,
      });
      prefs.setString('userData', _userData);
//      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> submitSignInRequest({
    @required String email,
    @required String password,
  }) async {
    final requestBody = {
      'email': email,
      'password': password,
    };

    final requestUrl = '/auth/sign_in';

    final headers = {
      'Content-Type': 'application/json',
    };
    try {
      final responseObject = await HttpRequests().postRequest(
        headers: headers,
        body: requestBody,
        requestUrl: requestUrl,
      );
      if (responseObject.containsKey('errors')) {
        var errorList = responseObject['errors'] as List<dynamic>;
        throw HttpExceptions(errorList);
      }
      _authToken = responseObject['Authorization'];
      var _userObject = responseObject['user'] as Map<String, Object>;

      _user = {
        'id': _userObject['id'],
        'email': _userObject['email'],
        'name': _userObject['name'],
        'createdAt': _userObject['created_at'],
      };
      _user = _userObject;
      final prefs = await SharedPreferences.getInstance();
      final _userData = json.encode({
        'Authorization': _authToken,
        'user': _user,
      });
      prefs.setString('userData', _userData);
//      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<bool> tryAutoLogin() async {

      final prefs = await SharedPreferences.getInstance();

      if (!prefs.containsKey('userData')) {
//        List<dynamic> errorList = [
//          'Could not find saved login information. Please login again.'
//        ];
//        throw HttpExceptions(errorList);
      return false;
      }

      final extractedUserData =
          json.decode(prefs.getString('userData')) as Map<String, Object>;

      _authToken = extractedUserData['Authorization'];
      _user = extractedUserData['user'];
      return true;
//    notifyListeners();
  }

  Future<void> submitLogOutRequest() async {
//    final requestUrl = '/auth/sign_out';

    print('inside provider log out, token: $_authToken');

//    final headers = {
//      'Content-Type': 'application/json',
//      'Authorization': _authToken,
//    };
    try {
//      final responseObject = await HttpRequests().deleteRequest(
//        headers: headers,
//        requestUrl: requestUrl,
//      );
//      if (responseObject.containsKey('errors')) {
//        var errorList = responseObject['errors'] as List<dynamic>;
//        throw HttpExceptions(errorList);
//      }
      _authToken = null;
      _user = null;
      final prefs = await SharedPreferences.getInstance();
//     prefs.remove('userData'); TIP: use when u need to remove selected data only
      prefs.clear();
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }
}
