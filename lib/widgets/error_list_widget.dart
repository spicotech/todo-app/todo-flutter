import 'package:flutter/material.dart';
import 'package:todo_spicotech/helpers/size_config.dart';

class ErrorListWidget extends StatelessWidget {
  final List<dynamic> errorMessages;

  const ErrorListWidget({
    @required this.errorMessages,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
//        color: Colors.red[100],
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: errorMessages.length,
              itemBuilder: (ctx, index) => Padding(
                padding: const EdgeInsets.all(8),
                child: Row(
                  children: <Widget>[
                    const Icon(
                      Icons.error,
                      color: Colors.red,
                    ),
                    const SizedBox(
                      width: 4,
                    ),
                    Expanded(
                      child: Text(
                        errorMessages[index].toString(),
//                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: SizeConfig.smallDevice ? 8 : 13,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig.smallDevice ? 7 : 15,
            ),
            Container(
              height: SizeConfig.smallDevice ? 20 : 36,
              child: FlatButton(
                color: Colors.green,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                onPressed: () => Navigator.of(context).pop(),
                child: Text(
                  'Dismiss',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
